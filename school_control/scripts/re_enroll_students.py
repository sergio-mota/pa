def main(args):
    from school_control.models import Group

    group = Group.objects.get(
        academic_program = args.academic_program,
        period = args.period,
        degree = args.degree,
        letter = args.letter
    )

    re_enroll_students(group)


def re_enroll_students(group):
    from school_control.models import (
        Course,
        Student,
        StudentRegistration,
        StudentInCourse,
        StudentInCourseStatus,
        UnregisteredStudent
    )

    from interface_xls import (
        read_students_data,
        read_student_in_courses_data
    )

    student_registration_registered = StudentRegistration.objects.get(name='Inscrito')
    student_status_regular = StudentInCourseStatus.objects.get(name='Ordinario')

    students_data = read_students_data(group)

    message = '{:d} students found.'
    print(message.format(len(students_data)))

    print('Re-enrolling students:')
    for index, student_data in enumerate(students_data):
        print(u'\t{:s}'.format(student_data.name))

        student = Student.objects.get(registration_number=student_data.registration_number)

        unregistered_student = UnregisteredStudent.objects.filter(student=student)
        if len(unregistered_student) != 0:
            message = '<{!s:s}> is not unregistered.'
            # raise Exception(message.format(student))
            print(message.format(student))
            continue

        student.registration = student_registration_registered
        student.save()

        group.student.add(student)

        student_in_courses = read_student_in_courses_data(group, index)
        for student_in_course in student_in_courses:
            print(u'\t\t{:s}\t{:s}'.format(student_in_course.subject_name, student_in_course.letter))

            subject = utils.convert_subject(student_in_course.subject_name)
            letter = utils.extract_letter(student_in_course.letter)
            oportunity = utils.convert_student_in_course_oportunity(student_in_course.oportunity_name)

            if 'UNICO' in letter:
                letter = '-'

            course = Course.objects.get(
                subject = subject,
                period  = group.period,
                letter  = letter
            )

            StudentInCourse.objects.create(
                course      = course,
                student     = student,
                oportunity  = oportunity,
                status      = student_status_regular
            )

if __name__ == '__main__':
    import argparse
    import django
    import os

    import utils

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'siu.settings')
    django.setup()

    parser = argparse.ArgumentParser(description='Re-enroll students.')
    parser.add_argument(
        'academic_program',
        help = 'Academic program',
        type = utils.convert_academic_program
    )
    parser.add_argument(
        'period',
        help = 'Period',
        type = utils.convert_period
    )
    parser.add_argument(
        'degree',
        help = 'Degree',
        type = utils.extract_integer
    )
    parser.add_argument(
        'letter',
        help = 'Letter',
        type = utils.extract_letter
    )

    args = parser.parse_args()
    main(args)