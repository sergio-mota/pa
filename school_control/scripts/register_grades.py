def main(args):
    from defs import FileType

    if args.file_type == FileType.grades_summary:
        register_grades_from_summary(
            args.academic_program,
            args.grade_type,
            args.period
        )


def register_grades_from_summary(
    academic_program,
    grade_type,
    period
):
    from decimal import Decimal

    from school_control.models import (
        Course,
        StudentInCourse
    )

    from defs import (
        FileType,
        GradeType
    )
    from interface_xls import read_grades_data

    grades_data = read_grades_data(
        academic_program,
        period,
        FileType.grades_summary,
        grade_type
    )

    message = '{:d} grades found.'
    print(message.format(len(grades_data)))

    for grade_data in grades_data:
        print(u'\t{:d}\t{:s}\t{:s}'.format(grade_data.student_registration_number, grade_data.subject_name, grade_data.letter))

        course = Course.objects.get(
            subject = utils.convert_subject(grade_data.subject_name),
            period  = period,
            letter  = utils.extract_letter(grade_data.letter)
        )

        student_in_course = StudentInCourse.objects.get(
            course  = course,
            student = utils.convert_student(grade_data.student_registration_number)
        )

        if grade_type == GradeType.first:
            student_in_course.first_grade = Decimal(grade_data.grade)
        elif grade_type == GradeType.second:
            student_in_course.second_grade = Decimal(grade_data.grade)
        elif grade_type == GradeType.regular:
            student_in_course.regular_grade = Decimal(grade_data.grade)
        elif grade_type == GradeType.final:
            student_in_course.final_grade = Decimal(grade_data.grade)

            student_in_course.status = utils.convert_student_in_course_status(grade_data.student_status)

            try:
                student_in_course.absenses = Decimal(grade_data.absenses)
            except:
                student_in_course.absenses = None
        else:
            message = 'Unsupported grade type <{:s}>.'
            raise Exception(message.format(grade_type))

        student_in_course.save()

if __name__ == '__main__':
    import argparse
    import django
    import os

    import utils

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'siu.settings')
    django.setup()

    parser = argparse.ArgumentParser(description='Register students.')
    parser.add_argument(
        'academic_program',
        help = 'Academic program',
        type = utils.convert_academic_program
    )
    parser.add_argument(
        'period',
        help = 'Period',
        type = utils.convert_period
    )
    parser.add_argument(
        'file_type',
        help = 'File type',
        type = utils.convert_file_type
    )
    parser.add_argument(
        'grade_type',
        help = 'Grade type',
        type = utils.convert_grade_type
    )

    args = parser.parse_args()
    main(args)