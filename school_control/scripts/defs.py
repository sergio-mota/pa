from collections import namedtuple
from enum import Enum


Cell = namedtuple('Cell', 'column row')
GradeData = namedtuple(
    'GradeData',
    'subject_name grade academic_program student_status absenses letter student_registration_number'
)
OfferedCourseData = namedtuple(
    'CourseData',
    'letter professor_name subject_name'
)
StudentData = namedtuple(
    'StudentData',
    'gender_key registration_number name'
)
StudentInCourseData = namedtuple(
    'StudentInCourseData',
    'subject_name degree letter oportunity_name'
)

class DataType(Enum):
    grade               = 1
    offered_course      = 2
    student             = 3
    student_in_course   = 4

class FileType(Enum):
    academic_load   = 1
    grades_summary  = 2
    offered_courses = 3

class GradeType(Enum):
    first   = 1
    second  = 2
    regular = 3
    final   = 4

def get_columns(cells):
    columns = []
    for field in cells._fields:
        attribute = getattr(cells, field)
        if attribute is None:
            columns.append(None)
        else:
            columns.append(attribute.column)

    return columns