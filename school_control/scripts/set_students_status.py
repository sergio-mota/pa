def main(args):
    set_students_status(args.academic_program, args.period)


def set_students_status(
    academic_program,
    period
):
    from school_control.models import (
        DeregistrationReason,
        DefinitiveDeregistration,
        Student,
        StudentInCourse,
        StudentRegistration,
        StudentStatus
    )

    students = Student.objects.filter(academic_program=academic_program)
    message = '{:d} students found.'
    print(message.format(len(students)))

    desertion_due_reproval = DeregistrationReason.objects.get(name='Reprobación')
    student_registration_registered = StudentRegistration.objects.get(name='Inscrito')
    student_registration_unregistered = StudentRegistration.objects.get(name='No inscrito')
    student_registration_definitive_deregistration = StudentRegistration.objects.get(name='Baja definitiva')
    student_status_irregular = StudentStatus.objects.get(name='Irregular')

    irregular_counter = 0
    definitive_deregistration_counter = 0
    for student in students:
        if student.registration == student_registration_registered:
            student.registration = student_registration_unregistered
            student.save()
        else:
            continue

        student_in_courses = StudentInCourse.objects.filter(
            course__period  = period,
            student         = student
        )
        student_in_courses_reproved = student_in_courses.filter(final_grade__lt=7.0)

        if len(student_in_courses_reproved) > 0:
            student.status = student_status_irregular
            student.save()

            irregular_counter += 1

        if len(student_in_courses_reproved) >= 4:
            message = u'{!s:s} - deregistration due to reproval'
            print(message.format(student))

            comment = u'Reprobó {:d} asignaturas: '.format(len(student_in_courses_reproved))
            for index, student_in_course_reproved in enumerate(student_in_courses_reproved):
                if index < len(student_in_courses_reproved) - 1:
                    comment += u'{!s:s}, '.format(student_in_course_reproved.course.subject)
                else:
                    comment += u'y {!s:s}.'.format(student_in_course_reproved.course.subject)

            DefinitiveDeregistration.objects.create(
                reason  = desertion_due_reproval,
                comment = comment,
                period  = period,
                student = student
            )

            student.registration = student_registration_definitive_deregistration
            student.save()

            definitive_deregistration_counter += 1

    message = '{:d} irregular students\n{:d} deregistered students due to reproval'
    print(message.format(irregular_counter, definitive_deregistration_counter))

if __name__ == '__main__':
    import argparse
    import django
    import os

    import utils

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'siu.settings')
    django.setup()

    parser = argparse.ArgumentParser(description='Set students status.')
    parser.add_argument(
        'academic_program',
        help = 'Academic program',
        type = utils.convert_academic_program
    )
    parser.add_argument(
        'period',
        help = 'Period',
        type = utils.convert_period
    )

    args = parser.parse_args()
    main(args)