def convert_academic_program(key):
    from school_control.models import AcademicProgram

    academic_program = AcademicProgram.objects.get(key__iexact=key)

    return academic_program

def convert_file_type(name):
    from defs import FileType

    if name == 'grades_summary':
        file_type = FileType.grades_summary
    else:
        message = 'Unsupported file type <{:s}>.'
        raise Exception(message.format(name))

    return file_type

def convert_grade_type(name):
    from defs import GradeType

    if name == 'first_grade':
        grade_type = GradeType.first
    elif name == 'second_grade':
        grade_type = GradeType.second
    elif name == 'regular_grade':
        grade_type = GradeType.regular
    elif name == 'final_grade':
        grade_type = GradeType.final
    else:
        message = 'Unsupported grade type <{:s}>.'
        raise Exception(message.format(name))

    return grade_type

def convert_gender(key):
    from miscellaneous.models import Gender

    gender = Gender.objects.get(name__startswith=key)

    return gender

def convert_period(key):
    from miscellaneous.models import Period

    period = Period.objects.get(key=key)

    return period

def convert_professor(name):
    from school_control.models import Professor

    professor = Professor.objects.get(name=name)

    return professor

def convert_student(registration_number):
    from school_control.models import Student

    student = Student.objects.get(registration_number=registration_number)

    return student

def convert_student_in_course_oportunity(key):
    import re

    from school_control.models import StudentInCourseOportunity

    key = re.sub('[.]', '', key)
    if key == 'Reg':
        student_in_course_oportunity = StudentInCourseOportunity.objects.get(name='Ordinaria')
    else:
        student_in_course_oportunity = StudentInCourseOportunity.objects.get(name__startswith=key)

    return student_in_course_oportunity

def convert_student_in_course_status(text):
    from school_control.models import StudentInCourseStatus

    text = strip_accents(text)

    if 'COM' in text:
        student_in_curso_status = StudentInCourseStatus.objects.get(name='Competencias')
    elif 'ORD' in text:
        student_in_curso_status = StudentInCourseStatus.objects.get(name='Ordinario')
    elif 'REG' in text:
        student_in_curso_status = StudentInCourseStatus.objects.get(name='Regular')
    else:
        message = 'Failed conversion to student in course status <{:s}>'
        raise Exception(message.format(text))

    return student_in_curso_status

def convert_subject(name):
    from school_control.models import Subject

    subject = Subject.objects.get(name=name)

    return subject

def extract_integer(text):
    try:
        text = strip_accents(text)
        integer = ''.join(character for character in text if character.isdigit())
    except:
        integer = text

    return int(integer)

def extract_letter(text):
    text = strip_accents(text)

    if 'ISW' in text:
        text = text.replace(' ', '')
        text = text.replace('-', '')
        text = text.replace('ISW', '')
    if 'lSW' in text:
        text = text.replace(' ', '')
        text = text.replace('-', '')
        text = text.replace('lSW', '')
    if 'MIXTO' in text:
        text = text.replace(' ', '')
        text = text.replace('MIXTO', '')
    if 'MIX' in text:
        text = text.replace(' ', '')
        text = text.replace('MIX', '')

    letter = ''.join(character for character in text if not character.isdigit())

    return letter

def strip_accents(string):
    import unicodedata

    string = ''.join(character for character in unicodedata.normalize('NFKD', string) if unicodedata.category(character) != 'Mn')

    return string