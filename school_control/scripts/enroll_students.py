def main(args):
    from school_control.models import Group

    group = Group.objects.get(
        academic_program    = args.academic_program,
        period              = args.period,
        degree              = 1,
        letter              = args.letter
    )

    register_students(group)


def register_students(group):
    from school_control.models import (
        Course,
        Generation,
        Student,
        StudentInCourse,
        StudentInCourseOportunity,
        StudentInCourseStatus,
        StudentRegistration,
        StudentStatus
    )

    from interface_xls import (
        read_student_in_courses_data,
        read_students_data
    )

    students_data = read_students_data(group)

    message = '{:d} students found.'
    print(message.format(len(students_data)))

    student_registration_registered = StudentRegistration.objects.get(name='Inscrito')
    student_status_regular = StudentStatus.objects.get(name='Ordinario')
    student_in_course_oportunity_ordinary = StudentInCourseOportunity.objects.get(name='Ordinaria')
    student_in_course_status_ordinary = StudentInCourseStatus.objects.get(name='Ordinario')

    print('Registering students:')
    for index, student_data in enumerate(students_data):
        print(u'\t{:s}'.format(student_data.name))

        if student_data.registration_number == 213030204:
            continue

        student = Student.objects.create(
            academic_program    = group.academic_program,
            gender              = utils.convert_gender(student_data.gender_key),
            generation          = Generation.objects.get(period=group.period),
            name                = student_data.name,
            registration_number = student_data.registration_number,
            registration        = student_registration_registered,
            status              = student_status_regular
        )
        group.student.add(student)

        student_in_courses = read_student_in_courses_data(group, index)
        for student_in_course in student_in_courses:
            print(u'\r\t\t{:s}'.format(student_in_course.subject_name))

            subject = utils.convert_subject(student_in_course.subject_name)
            letter = utils.extract_letter(student_in_course.letter)

            course = Course.objects.get(
                letter  =   letter,
                period  =   group.period,
                subject =   subject
            )

            StudentInCourse.objects.create(
                course =        course,
                oportunity =    student_in_course_oportunity_ordinary,
                status =        student_in_course_status_ordinary,
                student =       student
            )

if __name__ == '__main__':
    import argparse
    import django
    import os

    import utils

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'siu.settings')
    django.setup()

    parser = argparse.ArgumentParser(description='Register students.')
    parser.add_argument(
        'academic_program',
        help =  'Academic program',
        type =  utils.convert_academic_program
    )
    parser.add_argument(
        'period',
        help = 'Period',
        type = utils.convert_period
    )
    parser.add_argument(
        'letter',
        help = 'Letter',
        type = utils.extract_letter
    )

    args = parser.parse_args()
    main(args)