def adjust_register(
    register,
    data_type
):
    from defs import (
        DataType,
        GradeData,
        OfferedCourseData,
        StudentData,
        StudentInCourseData
    )
    from utils import (
        extract_integer,
        extract_letter
    )

    if data_type == DataType.grade:
        try:
            absenses = int(register[4])
        except:
            absenses = None

        data = GradeData(
            subject_name                = register[0],
            grade                       = float(register[1]),
            academic_program            = register[2],
            student_status              = register[3],
            absenses                    = absenses,
            letter                      = extract_letter(register[5]),
            student_registration_number = int(register[6])
        )
    elif data_type == DataType.offered_course:
        data = OfferedCourseData(
            letter =            extract_letter(register[0]),
            subject_name =      register[2],
            professor_name =    register[1]
        )
    elif data_type == DataType.student:
        data = StudentData(
            gender_key =            register[0],
            registration_number =   int(register[1]),
            name =                  register[2].title()
        )
    elif data_type == DataType.student_in_course:
        data = StudentInCourseData(
            degree =            extract_integer(register[1]),
            letter =            extract_letter(register[2]),
            oportunity_name =   register[3],
            subject_name =      register[0].title()

        )
    else:
        message = 'Unsupported data type <{:s}>.'
        raise Exception(message.format(data_type))

    return data

def adjust_registers(
    registers,
    data_type
):
    data = []
    for register in registers:
        data.append(adjust_register(register, data_type))

    return data

def build_filepath(
    academic_program    = None,
    file_type           = None,
    group               = None,
    period              = None
):
    from defs import FileType
    from django.conf import settings
    import os

    dir_sgc = settings.STATIC_ROOT + '/sgc/'

    if file_type == FileType.academic_load:
        subdir = 'soporte/sac15-po-01/sac01-rg-24/'
        filename = '{:s}-{:s}-{:d}{:s}.xls'
        filename = filename.format(
            group.academic_program.key,
            group.period.key,
            group.degree,
            group.letter
        )
    elif file_type == FileType.grades_summary:
        subdir = 'clave/sac01-po-02/grades_summary/'
        filename = '{:s}.xls'
        filename = filename.format(period.key)
    elif file_type == FileType.offered_courses:
        subdir = 'clave/sac01-po-01/sac01-rg-01/'
        filename = '{:s}-{:s}.xls'
        filename = filename.format(
            academic_program.key,
            period.key
        )
    else:
        mensaje = 'Unsupported file type <{:s}>.'
        raise Exception(mensaje.format(file_type))

    return os.path.join(os.path.join(dir_sgc, subdir), filename.lower())

def get_cells(
    period      = None,
    data_type   = None,
    file_type   = None,
    grade_type  = None
):
    from defs import (
        Cell,
        DataType,
        FileType,
        GradeType,
        GradeData,
        OfferedCourseData,
        StudentData,
        StudentInCourseData
    )

    if file_type == FileType.academic_load:
        if data_type == DataType.student:
            cells = StudentData(
                gender_key =            Cell(3, 7),
                registration_number =   Cell(1, 7),
                name =                  Cell(2, 7)
            )
        elif data_type == DataType.student_in_course:
            cells = StudentInCourseData(
                degree =            Cell(4, 12),
                letter =            Cell(4, 12),
                oportunity_name =   Cell(3, 12),
                subject_name =      Cell(0, 12)
            )
        else:
            message = 'Unsupported data type <{:s}>.'
            raise Exception(message.format(data_type))
    elif file_type == FileType.grades_summary:
        if period.start.year == 2013:
            cells = GradeData(
                subject_name                = Cell(5, 3),
                grade                       = None,
                academic_program            = Cell(4, 3),
                student_status              = None,
                absenses                    = None,
                letter                      = Cell(6, 3),
                student_registration_number = Cell(1, 3)
            )

            if grade_type == GradeType.first:
                cells = cells._replace(
                    grade           = Cell(7, 3),
                    student_status  = None,
                    absenses        = Cell(8, 3)
                )
            elif grade_type == GradeType.final:
                cells = cells._replace(
                    grade           = Cell( 8, 3),
                    student_status  = Cell(11, 3),
                    absenses        = Cell( 9, 3)
                )
            else:
                message = 'Unsupported grade type <{:s}>.'
                raise Exception(message.format(grade_type))
        elif period.start.year == 2014:
            if period.start.month == 1:
                cells = GradeData(
                    subject_name                = Cell(5, 1),
                    grade                       = None,
                    academic_program            = Cell(3, 1),
                    student_status              = None,
                    absenses                    = None,
                    letter                      = Cell(4, 1),
                    student_registration_number = Cell(0, 1)
                )

                if grade_type == GradeType.first:
                    cells = cells._replace(
                        grade           = Cell(6, 1),
                        student_status  = None,
                        absenses        = Cell(7, 1)
                    )
                elif grade_type == GradeType.second:
                    cells = cells._replace(
                        grade           = Cell(8, 1),
                        student_status  = None,
                        absenses        = Cell(9, 1)
                    )
                elif grade_type == GradeType.regular:
                    cells = cells._replace(
                        grade           = Cell(10, 1),
                        student_status  = None,
                        absenses        = None
                    )
                elif grade_type == GradeType.final:
                    cells = cells._replace(
                        grade           = Cell(11, 1),
                        student_status  = Cell(12, 1),
                        absenses        = Cell(13, 1)
                    )
                else:
                    message = 'Unsupported grade type <{:s}>.'
                    raise Exception(message.format(grade_type))
            if period.start.month == 5 or period.start.month == 9:
                cells = GradeData(
                    subject_name                = Cell(16, 1),
                    grade                       = None,
                    academic_program            = Cell( 1, 1),
                    student_status              = None,
                    absenses                    = None,
                    letter                      = Cell(15, 1),
                    student_registration_number = Cell( 2, 1)
                )

                if grade_type == GradeType.first:
                    cells = cells._replace(
                        grade           = Cell(4, 1),
                        student_status  = None,
                        absenses        = Cell(5, 1)
                    )
                elif grade_type == GradeType.second:
                    cells = cells._replace(
                        grade           = Cell(6, 1),
                        student_status  = None,
                        absenses        = Cell(7, 1)
                    )
                elif grade_type == GradeType.regular:
                    cells = cells._replace(
                        grade           = Cell(11, 1),
                        student_status  = None,
                        absenses        = None
                    )
                elif grade_type == GradeType.final:
                    cells = cells._replace(
                        grade           = Cell(12, 1),
                        student_status  = Cell(13, 1),
                        absenses        = Cell( 9, 1)
                    )
                else:
                    message = 'Unsupported grade type <{:s}>.'
                    raise Exception(message.format(grade_type))
            else:
                message = 'Unsupported period <{!s:s}>.'
                raise Exception(message.format(period))
        elif period.start.year in [2015, 2016]:
            if period.start.month in [1, 5, 9]:
                cells = GradeData(
                    subject_name                = Cell(16, 1),
                    grade                       = None,
                    academic_program            = Cell( 1, 1),
                    student_status              = None,
                    absenses                    = None,
                    letter                      = Cell(15, 1),
                    student_registration_number = Cell( 2, 1)
                )

                if grade_type == GradeType.first:
                    cells = cells._replace(
                        grade           = Cell(4, 1),
                        student_status  = None,
                        absenses        = Cell(5, 1)
                    )
                elif grade_type == GradeType.second:
                    cells = cells._replace(
                        grade           = Cell(6, 1),
                        student_status  = None,
                        absenses        = Cell(7, 1)
                    )
                elif grade_type == GradeType.regular:
                    cells = cells._replace(
                        grade           = Cell(11, 1),
                        student_status  = None,
                        absenses        = None
                    )
                elif grade_type == GradeType.final:
                    cells = cells._replace(
                        grade           = Cell(12, 1),
                        student_status  = Cell(13, 1),
                        absenses        = Cell( 9, 1)
                    )
                else:
                    message = 'Unsupported grade type <{:s}>.'
                    raise Exception(message.format(grade_type))
            else:
                message = 'Unsupported period <{!s:s}>.'
                raise Exception(message.format(period))
        else:
            message = 'Unsupported period <{!s:s}>.'
            raise Exception(message.format(period))
    elif file_type == FileType.offered_courses:
        cells = OfferedCourseData(
            letter =            Cell(0, 0),
            subject_name =      Cell(1, 0),
            professor_name =    Cell(2, 0)
        )
    else:
        message = 'Unsupported file type <{:s}>.'
        raise Exception(message.format(file_type))

    return cells

def get_sheet_name(
    data_type   = None,
    file_type   = None,
    grade_type  = None,
    period      = None
):
    from defs import (
        DataType,
        FileType,
        GradeType
    )

    if file_type == FileType.academic_load:
        if data_type == DataType.student:
            sheet_name = 'LISTAS'
        elif data_type == DataType.student_in_course:
            sheet_name = 'T'
        else:
            message = 'Unsupported data type <{:s}>'
            raise Exception(message.format(data_type))
    elif file_type == FileType.grades_summary:
        if period.start.year == 2013:
            if grade_type == GradeType.first:
                sheet_name = 'PARCIAL 1'
            elif grade_type == GradeType.final:
                sheet_name = 'FINAL'
            else:
                message = 'Unsupported grade type <{:s}>'
                raise Exception(message.format(grade_type))
        if period.start.year == 2014:
            if period.start.month == 1:
                sheet_name = 'Hoja1'
            elif period.start.month == 5:
                sheet_name = 'CALIF 142'
            elif period.start.month == 9:
                sheet_name = '143'
            else:
                print(period.start.month)
                message = 'Unsupported period <{!s:s}>'
                raise Exception(message.format(period))
        if period.start.year == 2015:
            if period.start.month == 1:
                sheet_name = '151'
            elif period.start.month == 5:
                sheet_name = 'Hoja1'
            elif period.start.month == 9:
                sheet_name = 'FINAL'
            else:
                print(period.start.month)
                message = 'Unsupported period <{!s:s}>'
                raise Exception(message.format(period))
        if period.start.year == 2016:
            if period.start.month in [1, 5, 9]:
                sheet_name = 'FINAL'
            else:
                print(period.start.month)
                message = 'Unsupported period <{!s:s}>'
                raise Exception(message.format(period))
        else:
            message = 'Unsupported period <{!s:s}>'
            raise Exception(message.format(period))
    elif file_type == FileType.offered_courses:
        sheet_name = 'Cursos'
    else:
        message = 'Unsupported file type <{:s}>'
        raise Exception(message.format(file_type))

    return sheet_name

def read_courses_data(
    academic_program,
    period
):
    import defs
    import xlrd

    file_type = defs.FileType.offered_courses

    filepath = build_filepath(
        academic_program=academic_program,
        file_type=file_type,
        period=period
    )
    workbook = xlrd.open_workbook(filepath)

    sheet_name = get_sheet_name(file_type=file_type)
    sheet = workbook.sheet_by_name(sheet_name)
    cells = get_cells(file_type=file_type)

    registers = read_rows(
        sheet=sheet,
        initial_row=cells[0].row+1,
        columns=defs.get_columns(cells)
    )
    courses_data = adjust_registers(
        data_type=defs.DataType.offered_course,
        registers=registers
    )

    return courses_data

def read_grades_data(
    academic_program,
    period,
    file_type,
    grade_type
):
    from defs import (
        DataType,
        FileType,
        get_columns
    )
    import xlrd

    if file_type == FileType.grades_summary:
        filepath = build_filepath(
            period      = period,
            file_type   = file_type
        )
        workbook = xlrd.open_workbook(filepath)

        sheet_name = get_sheet_name(
            period      = period,
            file_type   = file_type,
            grade_type  = grade_type,
            data_type   = DataType.grade
        )
        sheet = workbook.sheet_by_name(sheet_name)

        cells = get_cells(
            period      = period,
            file_type   = file_type,
            grade_type  = grade_type,
            data_type   = DataType.grade
        )

        registers = read_rows(
            sheet       = sheet,
            initial_row = cells[0].row + 1,
            columns     = get_columns(cells)
        )

        registers = [register for register in registers if register[2] == academic_program.key]
        registers = [register for register in registers if register[1] != '' and register[1] > 1e-3]

        grades_data = adjust_registers(
            registers,
            DataType.grade
        )
    else:
        message = 'Unsupported file type <{:s}>.'
        raise Exception(message.format(file_type))

    return grades_data

def read_rows(
    sheet,
    initial_row,
    columns,
    num_rows=-1
):
    import xlrd

    registers = []
    row = initial_row
    while True:
        if row >= sheet.nrows:
            break

        if num_rows >= 0 and row - initial_row > num_rows:
            break

        cell_type = sheet.cell_type(row, columns[0])
        if cell_type in (xlrd.XL_CELL_EMPTY, xlrd.XL_CELL_BLANK):
            if num_rows < 0:
                break
            else:
                row += 1
                continue

        tmp = []
        for column in columns:
            if column is None:
                tmp.append(None)
            else:
                value = sheet.cell(row, column).value
                try:
                    value = ' '.join(value.split())
                except:
                    value = value
                tmp.append(value)

        registers.append(tmp)
        row += 1

    return registers

def read_student_in_courses_data(
    group,
    index
):
    from defs import (
        DataType,
        FileType,
        get_columns
    )
    import xlrd

    data_type = DataType.student_in_course
    file_type = FileType.academic_load
    num_spaces_for_courses_in_academic_load = 9

    filepath = build_filepath(
        group =     group,
        file_type = file_type
    )
    workbook = xlrd.open_workbook(filepath)

    sheet_name = get_sheet_name(
        file_type = file_type,
        data_type = data_type
    )
    sheet_name += str(index + 1)
    sheet = workbook.sheet_by_name(sheet_name)
    cells = get_cells(
        file_type = file_type,
        data_type = data_type
    )

    registers = read_rows(
        sheet =         sheet,
        initial_row =   cells[0].row + 1,
        columns =       get_columns(cells),
        num_rows =      num_spaces_for_courses_in_academic_load
    )
    estudiante_cursos = adjust_registers(
        registers,
        data_type
    )

    return estudiante_cursos

def read_students_data(group):
    from defs import (
        DataType,
        FileType,
        get_columns
    )
    import xlrd

    data_type = DataType.student
    file_type = FileType.academic_load

    filepath = build_filepath(
        group =     group,
        file_type = file_type
    )
    workbook = xlrd.open_workbook(filepath)

    sheet_name = get_sheet_name(
        file_type = file_type,
        data_type = data_type
    )
    sheet = workbook.sheet_by_name(sheet_name)
    cells = get_cells(
        file_type = file_type,
        data_type = data_type
    )

    registers = read_rows(
        sheet =         sheet,
        initial_row =   cells[0].row + 1,
        columns =       get_columns(cells)
    )
    students_data = adjust_registers(
        registers,
        data_type
    )

    return students_data