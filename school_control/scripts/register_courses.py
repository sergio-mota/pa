def main(args):
    register_courses(args.academic_program, args.period)

def register_courses(
    academic_program,
    period
):
    from school_control.models import Course

    from interface_xls import read_courses_data

    courses_data = read_courses_data(academic_program, period)
    message = '{:d} courses found.'
    print(message.format(len(courses_data)))

    print('Registering courses:')
    for course_data in courses_data:
        print(u'\t{:s} - {:s}'.format(course_data.subject_name, course_data.professor_name))

        subject = utils.convert_subject(course_data.subject_name)
        professor = utils.convert_professor(course_data.professor_name)
        letter = utils.extract_letter(course_data.letter)

        Course.objects.create(
            subject =   subject,
            professor = professor,
            period =    period,
            letter =    letter
        )

if __name__ == '__main__':
    import argparse
    import django
    import os

    import utils

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'siu.settings')
    django.setup()

    parser = argparse.ArgumentParser(description='Register courses.')
    parser.add_argument(
        'academic_program',
        help = 'Academic program',
        type = utils.convert_academic_program
    )
    parser.add_argument(
        'period',
        help = 'Period',
        type = utils.convert_period
    )

    args = parser.parse_args()
    main(args)