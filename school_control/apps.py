from django.apps import AppConfig


class SchoolControlConfig(AppConfig):
    name = 'school_control'
