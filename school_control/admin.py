from django.contrib import admin

from school_control.models import (
    AcademicProgram,
    Course,
    CurricularAxis,
    CurricularMap,
    DefinitiveDeregistration,
    DeregistrationReason,
    Generation,
    Group,
    Professor,
    Scholarship,
    Student,
    StudentHasScholarship,
    StudentInCourse,
    StudentInCourseOportunity,
    StudentInCourseStatus,
    StudentRegistration,
    StudentStatus,
    Subject,
    TemporaryDeregistration,
    UnregisteredStudent,
)

# Register your models here.
class AcademicProgramAdmin(admin.ModelAdmin):
    list_display = ('key', 'name', 'logo',)
    ordering = ('name',)
admin.site.register(AcademicProgram, AcademicProgramAdmin)

class CourseAdmin(admin.ModelAdmin):
    list_display = ('subject', 'professor', 'period', 'letter',)
    ordering = ('period', 'subject', 'letter',)
admin.site.register(Course, CourseAdmin)

class CurricularAxisAdmin(admin.ModelAdmin):
    list_display = ('key', 'name',)
    ordering = ('name',)
admin.site.register(CurricularAxis, CurricularAxisAdmin)

class CurricularMapAdmin(admin.ModelAdmin):
    list_display = ('academic_program', 'date',)
    ordering = ('academic_program', 'date',)
admin.site.register(CurricularMap, CurricularMapAdmin)

class DefinitiveDeregistrationAdmin(admin.ModelAdmin):
    list_display = ('student', 'period', 'reason',)
    ordering = ('student', 'period',)
admin.site.register(DefinitiveDeregistration, DefinitiveDeregistrationAdmin)

class DeregistrationReasonAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(DeregistrationReason, DeregistrationReasonAdmin)

class GenerationAdmin(admin.ModelAdmin):
    list_display = ('period',)
    ordering = ('period',)
admin.site.register(Generation, GenerationAdmin)

class GroupAdmin(admin.ModelAdmin):
    list_display = ('academic_program', 'period', 'degree', 'letter', 'tutor',)
    ordering = ('academic_program', 'period', 'degree', 'letter',)
admin.site.register(Group, GroupAdmin)

class ProfessorAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'cv', 'phone')
    ordering = ('name',)
admin.site.register(Professor, ProfessorAdmin)

class ScholarshipAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(Scholarship, ScholarshipAdmin)

class StudentAdmin(admin.ModelAdmin):
    list_display = ('name', 'generation', 'registration', 'status',)
    ordering = ('name', 'generation',)
admin.site.register(Student, StudentAdmin)

class StudentHasScholarshipAdmin(admin.ModelAdmin):
    list_display = ('student', 'scholarship', 'period',)
    ordering = ('student', 'period',)
admin.site.register(StudentHasScholarship, StudentHasScholarshipAdmin)

class StudentInCourseAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'oportunity', 'status', 'first_grade', 'second_grade', 'regular_grade', 'final_grade', 'absenses',)
    ordering = ('student', 'course',)
admin.site.register(StudentInCourse, StudentInCourseAdmin)

class StudentInCourseOportunityAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(StudentInCourseOportunity, StudentInCourseOportunityAdmin)

class StudentInCourseStatusAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(StudentInCourseStatus, StudentInCourseStatusAdmin)

class StudentRegistrationAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(StudentRegistration, StudentRegistrationAdmin)

class StudentStatusAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(StudentStatus, StudentStatusAdmin)

class SubjectAdmin(admin.ModelAdmin):
    list_display = ('key', 'name', 'degree', 'practical_hours', 'theoretical_hours',)
    ordering = ('name', 'degree',)
admin.site.register(Subject, SubjectAdmin)

class TemporaryDeregistrationAdmin(admin.ModelAdmin):
    list_display = ('student', 'period', 'reason',)
    ordering = ('student', 'period',)
admin.site.register(TemporaryDeregistration, TemporaryDeregistrationAdmin)

class UnregisteredStudentAdmin(admin.ModelAdmin):
    list_display = ('student', 'period',)
    ordering = ('student', 'period',)
admin.site.register(UnregisteredStudent, UnregisteredStudentAdmin)