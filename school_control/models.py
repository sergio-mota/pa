from django.db import models

from miscellaneous.models import (
    EmployeeType,
    Gender,
    Period,
)

app_label = 'school_control'

# Create your models here.
class AcademicProgram(models.Model):
    key = models.CharField(
        max_length=4,
        unique=True
    )
    name = models.CharField(
        max_length=128,
        unique=True
    )
    logo = models.ImageField(
        blank=True,
        null=True,
        upload_to='logos'
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        unique_together = (('key', 'name'),)
        verbose_name = 'Programa académico'
        verbose_name_plural = 'Programas académicos'

    def __str__(self):
        return u'{:s}'.format(self.name)


class Course(models.Model):
    subject = models.ForeignKey('Subject')
    professor = models.ForeignKey('Professor')
    period = models.ForeignKey(Period)
    letter = models.CharField(max_length=1)

    student = models.ManyToManyField(
        'Student',
        through='StudentInCourse'
    )

    class Meta:
        app_label = app_label
        ordering = ['period', 'subject', 'letter',]
        unique_together = (('subject', 'period', 'letter',),)
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'

    def __str__(self):
        return u'{!s:s} - {!s:s} - {:s}'.format(self.subject, self.period, self.letter)


class CurricularAxis(models.Model):
    key = models.CharField(
        max_length=4,
        unique=True
    )
    name = models.CharField(
        max_length=64,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name',]
        verbose_name = 'Eje curricular'
        verbose_name_plural = 'Ejes curriculares'

    def __str__(self):
        return u'{:s}'.format(self.name)


class CurricularMap(models.Model):
    academic_program = models.ForeignKey(AcademicProgram)
    date = models.DateField()

    class Meta:
        app_label = app_label
        ordering = ['academic_program', 'date']
        unique_together = (('academic_program', 'date'),)
        verbose_name = 'Mapa curricular'
        verbose_name_plural = 'Mapas curriculares'

    def __str__(self):
        return u'{:s} - {:%Y %B}'.format(self.academic_program.key, self.date)

class DefinitiveDeregistration(models.Model):
    student = models.OneToOneField(
        'Student',
        primary_key=True
    )
    period = models.ForeignKey(Period)
    reason = models.ForeignKey('DeregistrationReason')

    comment = models.CharField(
        blank=True,
        max_length=256
    )

    class Meta:
        app_label = app_label
        ordering = ['student', 'period',]
        verbose_name = 'Baja definitiva'
        verbose_name_plural = 'Bajas definitivas'

    def __str__(self):
        return u'{!s:s} {!s:s}'.format(self.student, self.period)

class DeregistrationReason(models.Model):
    name = models.CharField(
        max_length=32,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name',]
        verbose_name = 'Causa de baja'
        verbose_name_plural = 'Causas de baja'

    def __str__(self):
        return u'{:s}'.format(self.name)

class Generation(models.Model):
    period = models.ForeignKey(Period)

    class Meta:
        app_label = app_label
        ordering = ['period']
        verbose_name = 'Generación'
        verbose_name_plural = 'Generaciones'

    def __str__(self):
        return u'{!s:s}'.format(self.period)

class Group(models.Model):
    academic_program = models.ForeignKey(AcademicProgram)
    period = models.ForeignKey(Period)

    degree = models.IntegerField()
    letter = models.CharField(max_length=1)
    tutor = models.ForeignKey('Professor')

    student = models.ManyToManyField(
        'Student',
        blank=True
    )

    class Meta:
        app_label = app_label
        ordering = ['academic_program', 'period', 'degree', 'letter',]
        unique_together = (('academic_program', 'period', 'degree', 'letter',),)
        verbose_name = 'Grupo'
        verbose_name_plural = 'Grupos'

    def __str__(self):
        # return u'{:s} - {:s} - {:d}{:s}'.format(self.academic_program.key, self.generation, self.degree, self.letter)
        return u'{:s} - {:d}{:s}'.format(self.academic_program.key, self.degree, self.letter)

class Professor(models.Model):
    name = models.CharField(max_length=64)
    initials = models.CharField(max_length=8)
    type = models.ForeignKey(EmployeeType)

    email = models.EmailField()
    cv = models.FilePathField(
        blank=True,
        null=True
    )

    gender = models.ForeignKey(Gender)
    employee_number = models.IntegerField(primary_key=True)
    phone = models.CharField(
        blank=True,
        max_length=15,
        null=True
    )

    active = models.BooleanField()
    academic_program = models.ManyToManyField(AcademicProgram)

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'

    def __str__(self):
        return u'{:s}'.format(self.name)

class Scholarship(models.Model):
    name = models.CharField(
        max_length=128,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Beca'
        verbose_name_plural = 'Becas'

    def __str__(self):
        return u'{:s}'.format(self.name)

class Student(models.Model):
    name = models.CharField(max_length=128)
    registration_number = models.IntegerField(primary_key=True)
    gender = models.ForeignKey(Gender)

    academic_program = models.ForeignKey(AcademicProgram)
    generation = models.ForeignKey(Generation)
    registration = models.ForeignKey('StudentRegistration')
    status = models.ForeignKey('StudentStatus')

    scholarship = models.ManyToManyField(
        Scholarship,
        blank=True,
        through='StudentHasScholarship'
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Estudiante'
        verbose_name_plural = 'Estudiantes'

    def __str__(self):
        return u'{:s}'.format(self.name)

class StudentHasScholarship(models.Model):
    student = models.ForeignKey('Student')
    scholarship = models.ForeignKey(Scholarship)
    period = models.ForeignKey(Period)

    class Meta:
        app_label = app_label
        ordering = ['period', 'student',]
        unique_together = (('student', 'scholarship', 'period',),)
        verbose_name = 'Beca de estudiante'
        verbose_name_plural = 'Becas de estudiantes'

    def __str__(self):
        return u'{:s} - {:s}'.format(self.student, self.scholarship, self.period)

class StudentInCourse(models.Model):
    student = models.ForeignKey(Student)
    course = models.ForeignKey(Course)

    oportunity = models.ForeignKey('StudentInCourseOportunity')
    status = models.ForeignKey('StudentInCourseStatus')

    first_grade = models.DecimalField(
        blank=True,
        decimal_places=2,
        max_digits=4,
        null=True
    )
    second_grade = models.DecimalField(
        blank=True,
        decimal_places=2,
        max_digits=4,
        null=True
    )
    regular_grade = models.DecimalField(
        blank=True,
        decimal_places=2,
        max_digits=4,
        null=True
    )
    final_grade = models.DecimalField(
        blank=True,
        decimal_places=2,
        max_digits=4,
        null=True
    )

    absenses = models.IntegerField(
        blank=True,
        null=True
    )

    class Meta:
        app_label = app_label
        ordering = ['student', 'course']
        unique_together = (('course', 'student'),)
        verbose_name = 'Estudiante en curso'
        verbose_name_plural = 'Estudiantes en cursos'

    def __str__(self):
        return u'{!s:s} - {!s:s}'.format(self.student, self.course)

class StudentInCourseOportunity(models.Model):
    name = models.CharField(
        max_length=16,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Oportunidad de estudiante en curso'
        verbose_name_plural = 'Oportunidades de estudiantes en cursos'

    def __str__(self):
        return u'{:s}'.format(self.name)

class StudentInCourseStatus(models.Model):
    name = models.CharField(
        max_length=16,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Estado de estudiante en curso'
        verbose_name_plural = 'Estados de estudiantes en cursos'

    def __str__(self):
        return u'{:s}'.format(self.name)

class StudentRegistration(models.Model):
    name = models.CharField(
        max_length=16,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Inscripción de estudiante'
        verbose_name_plural = 'Inscripciones de estudiantes'

    def __str__(self):
        return u'{:s}'.format(self.name)

class StudentStatus(models.Model):
    name = models.CharField(
        max_length=16,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Estado de estudiante'
        verbose_name_plural = 'Estados de estudiante'

    def __str__(self):
        return u'{:s}'.format(self.name)

class Subject(models.Model):
    key = models.CharField(max_length=8)
    name = models.CharField(max_length=64)
    degree = models.IntegerField()

    curricular_map = models.ForeignKey('CurricularMap')
    curricular_axis = models.ForeignKey('CurricularAxis')
    credits = models.IntegerField()

    practical_hours = models.IntegerField()
    theoretical_hours = models.IntegerField()

    prerequisite = models.ManyToManyField(
        'self',
        blank=True
    )

    class Meta:
        app_label = app_label
        ordering = ['degree', 'name',]
        unique_together = (('curricular_map', 'name',),)
        verbose_name = 'Asignatura'
        verbose_name_plural = 'Asignaturas'

    def __str__(self):
        return u'{:s}'.format(self.name)

class TemporaryDeregistration(models.Model):
    student = models.OneToOneField(
        Student,
        primary_key=True
    )
    period = models.ForeignKey(Period)
    reason = models.ForeignKey(DeregistrationReason)

    comment = models.CharField(
        blank=True,
        max_length=256
    )

    class Meta:
        app_label = app_label
        ordering = ['student', 'period',]
        verbose_name = 'Baja temporal'
        verbose_name_plural = 'Bajas temporales'

    def __str__(self):
        return u'{!s:s} {!s:s}'.format(self.student, self.period)

class UnregisteredStudent(models.Model):
    student = models.OneToOneField(
        Student,
        primary_key=True
    )
    period = models.ForeignKey(Period)

    class Meta:
        app_label = app_label
        ordering = ['student', 'period']
        verbose_name = 'Estudiante no inscrito'
        verbose_name_plural = 'Estudiantes no inscritos'

    def __unicode__(self):
        return u'{!s:s} {!s:s}'.format(self.cuatrimestre, self.estudiante)