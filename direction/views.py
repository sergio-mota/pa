from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/login/')
def direction(request):
    from django.shortcuts import render

    return render(request, 'direction.html')

@login_required(login_url='/login/')
def enrollment(request):
    from django.shortcuts import render

    from direction.forms import EnrollmentForm

    form = EnrollmentForm

    return render(
        request,
        'enrollment.html',
        {
            'form': form,
        }
    )


def get_google_chart_column_name(
    label,
    type,
    last = False
):
    string = '{'
    string +=   '"id": "", '
    string +=   '"label": "' + label + '", '
    string +=   '"pattern": "",'
    string +=   '"type": "' + type + '"'
    string += '}'

    if last is False:
        string += ', '

    return string


def get_google_chart_column_value(
    value,
    last = False
):
    string = '{'
    string += '"v" : "' + str(value) + '", '
    string += '"f" : null'
    string += '}'

    if last is False:
        string += ', '

    return string


def get_json_string(
    label,
    value,
    last = False
):
    string = '"' + label + '": "' + str(value) + '"'

    if last is False:
        string += ', '

    return string


@login_required(login_url='/login/')
def enrollment_ajax(request):
    from django.http import JsonResponse

    from miscellaneous.models import Period
    from school_control.models import (
        AcademicProgram,
        DefinitiveDeregistration,
        Generation,
        Student,
        TemporaryDeregistration,
        UnregisteredStudent
    )

    data_string = ''
    plot_string = ''

    if request.is_ajax() and request.method == 'POST':
        # Get form data
        academic_program_id = request.POST.get('academic_program')
        academic_program = AcademicProgram.objects.filter(id = academic_program_id)
        data_string += get_json_string('academic_program_id', academic_program_id)

        generations_id = request.POST.getlist('generations[]')
        generations_id = [int(id) for id in generations_id]
        generations = Generation.objects.filter(id__in = generations_id)

        percentage = request.POST.get('percentage') in ['true', '1']

        # Set periods of interest
        periods = Period.objects.all()
        periods = [period for period in periods if period >= generations[0].period]


        data_string += get_json_string('percentage', percentage)

        plot_string += '"chart_cols": ['
        plot_string += get_google_chart_column_name('Periodo', 'string')
        plot_string += get_google_chart_column_name('Inscritos', 'number')
        plot_string += get_google_chart_column_name('Bajas definitivas', 'number')
        plot_string += get_google_chart_column_name('Bajas temporales', 'number')
        plot_string += get_google_chart_column_name('No inscritos', 'number', True)
        plot_string += '], '

        data_string += '"periods": ['
        plot_string += '"chart_rows": ['

        first = True
        num_definitive_deregistrations = 0
        num_temporary_deregistrations = 0
        num_unregistered_students = 0

        for period in periods:
            if first is True:
                first = False
            else:
                data_string += ', '
                plot_string += ', '

            existing_generations = [generation for generation in generations if generation.period <= period]

            students = Student.objects.filter(
                academic_program    = academic_program,
                generation__in      = existing_generations
            )

            if percentage:
                scale_factor = 100.0 / len(students)
            else:
                scale_factor = 1

            num_registered_students = len(students) - num_definitive_deregistrations - num_temporary_deregistrations - num_unregistered_students

            definitive_deregistrations = DefinitiveDeregistration.objects.filter(
                period                      = period,
                student__academic_program   = academic_program,
                student__generation__in     = existing_generations
            )
            num_definitive_deregistrations += len(definitive_deregistrations)

            temporary_deregistrations = TemporaryDeregistration.objects.filter(
                period                      = period,
                student__academic_program   = academic_program,
                student__generation__in     = existing_generations
            )
            num_temporary_deregistrations += len(temporary_deregistrations)

            unregistered_students = UnregisteredStudent.objects.filter(
                period                      = period,
                student__academic_program   = academic_program,
                student__generation__in     = existing_generations
            )
            num_unregistered_students += len(unregistered_students)

            registered_students_value = num_registered_students * scale_factor
            definitive_deregistrations_value = num_definitive_deregistrations * scale_factor
            temporary_deregistrations_value = num_temporary_deregistrations * scale_factor
            unregistered_students_value = num_unregistered_students * scale_factor

            data_string += '{'
            data_string += get_json_string('key', period.key)
            data_string += get_json_string('num_registered_students', registered_students_value)
            data_string += get_json_string('num_definitive_deregistrations', definitive_deregistrations_value)
            data_string += get_json_string('num_temporary_deregistrations', temporary_deregistrations_value)
            data_string += get_json_string('num_unregistered_students', unregistered_students_value, True)
            data_string += '}'

            plot_string += '{"c": ['
            plot_string += get_google_chart_column_value(period.key)
            plot_string += get_google_chart_column_value(registered_students_value)
            plot_string += get_google_chart_column_value(definitive_deregistrations_value)
            plot_string += get_google_chart_column_value(temporary_deregistrations_value)
            plot_string += get_google_chart_column_value(unregistered_students_value, True)
            plot_string += ']}'

        data_string += ']'
        plot_string += ']'

    to_json = '{' + data_string + ', ' + plot_string + '}'
    return JsonResponse(to_json, safe=False)


@login_required(login_url='/login/')
def reproval(request):
    from django.shortcuts import render

    from direction.forms import ReprovalForm

    form = ReprovalForm

    return render(
        request,
        'reproval.html',
        {
            'form': form,
        }
    )


@login_required(login_url='/login/')
def reproval_ajax(request):
    from django.http import JsonResponse

    from miscellaneous.models import Period
    from school_control.models import (
        AcademicProgram,
        Course,
        StudentInCourse
    )

    data_string = ''
    plot_string = ''

    if request.is_ajax() and request.method == 'POST':
        # Get model instances identifiers
        academic_program_id = request.POST.get('academic_program')
        academic_program = AcademicProgram.objects.filter(id = academic_program_id)

        period_id = request.POST.get('period')
        period = Period.objects.filter(id = period_id)

        degree_id = request.POST.get('degree')
        degree = int(degree_id)

        courses = Course.objects.filter(
            period                                      = period,
            subject__curricular_map__academic_program   = academic_program,
            subject__degree                             = degree
        )

        data_string += '"courses": ['

        plot_string += '"chart_cols": ['
        plot_string += get_google_chart_column_name('Curso', 'string')
        plot_string += get_google_chart_column_name('Primer parcial', 'number')
        plot_string += get_google_chart_column_name('Segundo parcial', 'number')
        plot_string += get_google_chart_column_name('Final', 'number', True)
        plot_string += '], '

        plot_string += '"chart_rows": ['

        first = True
        for course in courses:
            if first is True:
                first = False
            else:
                data_string += ', '
                plot_string += ', '

            students_in_course = StudentInCourse.objects.filter(course = course)
            num_students_in_course = len(students_in_course)

            reproved_first = students_in_course.filter(first_grade__lt = 7.0)
            num_reproved_first = len(reproved_first)

            reproved_second = students_in_course.filter(second_grade__lt = 7.0)
            num_reproved_second = len(reproved_second)

            reproved_final = students_in_course.filter(final_grade__lt = 7.0)
            num_reproved_final = len(reproved_final)

            key = course.subject.key + ' ' + course.letter
            percentage_reproved_first = (100.0 * num_reproved_first) / num_students_in_course
            percentage_reproved_second = (100.0 * num_reproved_second) / num_students_in_course
            percentage_reproved_final = (100.0 * num_reproved_final) / num_students_in_course

            data_string += '{'
            data_string += get_json_string('key', key)
            data_string += get_json_string('percentage_reproved_first', percentage_reproved_first)
            data_string += get_json_string('percentage_reproved_second', percentage_reproved_second)
            data_string += get_json_string('percentage_reproved_final', percentage_reproved_final, True)
            data_string += '}'

            plot_string += '{"c": ['
            plot_string += get_google_chart_column_value(key)
            plot_string += get_google_chart_column_value(percentage_reproved_first)
            plot_string += get_google_chart_column_value(percentage_reproved_second)
            plot_string += get_google_chart_column_value(percentage_reproved_final, True)
            plot_string += ']}'

        data_string += ']'
        plot_string += ']'

    to_json = '{' + data_string + ', ' + plot_string + '}'
    return JsonResponse(to_json, safe=False)