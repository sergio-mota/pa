from django.conf.urls import url

from direction.views import (
    direction,
    enrollment,
    enrollment_ajax,
    reproval,
    reproval_ajax
)

urlpatterns = [
    url(r'^$', direction),

    url(r'^enrollment/$', enrollment),
    url(r'^enrollment-ajax/$',
        enrollment_ajax,
        name='direction-enrollment-ajax'
    ),

    url(r'^reproval/$', reproval),
    url(r'^reproval-ajax/$',
        reproval_ajax,
        name='direction-reproval-ajax'
    ),
]