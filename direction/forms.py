from django import forms

class EnrollmentForm(forms.Form):
    from school_control.models import (
        AcademicProgram,
        Generation
    )

    academic_program = forms.ModelChoiceField(
        empty_label = 'Seleccione un programa académico',
        label       = 'Programa académico',
        queryset    = AcademicProgram.objects.all(),
        widget      = forms.Select(attrs={'class': 'form-control'})
    )

    generations = forms.ModelMultipleChoiceField(
        label       = 'Generaciones',
        queryset    = Generation.objects.all(),
        widget      = forms.SelectMultiple(attrs={'class': 'form-control'})
    )

    percentage = forms.BooleanField(
        label       = 'Porcentaje',
        required    = False,
        widget      = forms.CheckboxInput(attrs={'class': 'form-control'})
    )


class ReprovalForm(forms.Form):
    from miscellaneous.models import Period
    from school_control.models import AcademicProgram

    academic_program = forms.ModelChoiceField(
        empty_label = 'Seleccione un programa académico',
        label       = 'Programa académico',
        queryset    = AcademicProgram.objects.all(),
        widget      = forms.Select(attrs={'class': 'form-control'})
    )

    period = forms.ModelChoiceField(
        empty_label = 'Seleccione un periodo',
        label       = 'Periodo',
        queryset    = Period.objects.all(),
        widget      = forms.Select(attrs={'class': 'form-control'})
    )

    DEGREE_CHOICES = [(i, i) for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
    DEGREE_CHOICES = [(0, 'Seleccione un grado')] + DEGREE_CHOICES
    degree = forms.ChoiceField(
        label   = 'Grados',
        choices = DEGREE_CHOICES,
        widget  = forms.Select(attrs={'class': 'form-control'})
    )