<!--
$(document).ready(function() {
    // $("#id_form").toggle();
    // $("#id_form").show();

    // $("#id_table").toggle();
    // $("#id_table").hide();
});

function get_enrollment_info() {
    console.log("get_enrollment_info()");

    var generations = document.getElementById("id_generations");
    var num_generations = 0;
    for (var i = 0; i < generations.options.length; i++) {
        if (generations.options[i].selected == true) {
            ++num_generations;
        }
    }
    if (num_generations == 0) {
        return;
    }

    console.log("entering ajax");
    $.ajax({
        url : "/direction/enrollment-ajax/",
        type : "POST",
        data : {
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            academic_program : $('#id_academic_program').val(),
            generations : $('#id_generations').val(),
            percentage : $('#id_percentage').is(":checked")
        },
        dataType: 'json',

        success : function(json) {
            console.log(json);
            var data = JSON.parse(json);

            $("#id_data_table tr").remove();

            var table = document.getElementById("id_data_table");

            var row = table.insertRow(0);
            var cell        = row.insertCell(0);
            cell.innerHTML  = "<b>Período</b>";
            cell            = row.insertCell(1);
            cell.innerHTML  = "<b>Número de estudiantes</b>";
            cell            = row.insertCell(2);
            cell.innerHTML  = "<b>Bajas definitivas</b>";
            cell            = row.insertCell(3);
            cell.innerHTML  = "<b>Bajas temporales</b>";
            cell            = row.insertCell(4);
            cell.innerHTML  = "<b>No inscritos</b>";

            for (var index in data.periods) {
                var period = data.periods[index]

                row = table.insertRow(-1);
                cell            = row.insertCell(0);
                cell.innerHTML  = period.key;
                cell            = row.insertCell(1);
                cell.innerHTML  = period.num_registered_students;
                cell            = row.insertCell(2);
                cell.innerHTML  = period.num_definitive_deregistrations;
                cell            = row.insertCell(3);
                cell.innerHTML  = period.num_temporary_deregistrations;
                cell            = row.insertCell(4);
                cell.innerHTML  = period.num_unregistered_students;
            }

            if (data.percentage == 'True') {
                vertical_axis_title = 'Porcentaje'
            } else {
                vertical_axis_title = 'Número de estudiantes'
            }

            var options = {
                height: 320,
                // width: 480,
                title: 'Matrícula',
                legend: {
                    position: 'bottom'
                },
                hAxis: {
                    title: 'Periodo'
                },
                vAxis: {
                    minValue: 0,
                    title: vertical_axis_title
                }
            };

            data.cols = data.chart_cols;
            data.rows = data.chart_rows;
            var chart = new google.visualization.LineChart(document.getElementById('id_chart'));
            var data_table = new google.visualization.DataTable(data);
            if (data_table.getNumberOfRows() > 0) {
                chart.draw(data_table, options);
            } else {
                $('#chart').html('');
            }

            console.log("success"); // sanity check
        },

        // handle a non-successful response
        error : function(xhr, errmsg, err) {
            $('#results').html(
                "<div class='alert-box alert radius' data-alert>" +
                    "Oops! We have encountered an error: " + errmsg +
                    " <a href='#' class='close'>&times;</a>" +
                "</div>"
            );
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
};
// -->