<!--
$(document).ready(function() {
    $("#id_form").toggle();
    $("#id_form").show();

    $("#id_table").toggle();
    $("#id_table").hide();
});

function get_reproval_info() {
    console.log("get_reproval_info()");

    console.log("entering ajax");
    $.ajax({
        url : "/direction/reproval-ajax/",
        type : "POST",
        data : {
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
            academic_program : $('#id_academic_program').val(),
            period : $('#id_period').val(),
            degree : $('#id_degree').val()
        },
        dataType: 'json',

        success : function(json) {
            console.log(json);
            var data = JSON.parse(json);

            $("#id_data_table tr").remove();

            var table = document.getElementById("id_data_table");

            var row = table.insertRow(0);
            var cell                = row.insertCell(0);
            cell.innerHTML          = "<b>Curso</b>";
            cell                    = row.insertCell(1);
            cell.innerHTML          = "<b>Primer parcial</b>";
            cell.style.textAlign    = 'center';
            cell                    = row.insertCell(2);
            cell.innerHTML          = "<b>Segundo parcial</b>";
            cell.style.textAlign    = 'center';
            cell                    = row.insertCell(3);
            cell.innerHTML          = "<b>Final</b>";
            cell.style.textAlign    = 'center';

            for (var index in data.courses) {
                var course = data.courses[index]

                row = table.insertRow(-1);
                cell                    = row.insertCell(0);
                cell.innerHTML          = course.key;
                cell                    = row.insertCell(1);
                cell.innerHTML          = Number(course.percentage_reproved_first).toFixed(1);
                cell.style.textAlign    = 'center';
                cell                    = row.insertCell(2);
                cell.innerHTML          = Number(course.percentage_reproved_second).toPrecision(3);
                cell.style.textAlign    = 'center';
                cell                    = row.insertCell(3);
                cell.innerHTML          = Number(course.percentage_reproved_final).toPrecision(3);
                cell.style.textAlign    = 'center';
            }

            var options = {
                height: 320,
                // width: 1366,
                title: 'Reprobación',
                legend: {
                    position: 'bottom'
                },
                hAxis: {
                    title: 'Curso'
                },
                vAxis: {
                    maxValue: 100,
                    minValue: 0,
                    title: 'Porcentaje'
                }
            };

            data.cols = data.chart_cols;
            data.rows = data.chart_rows;
            var chart = new google.visualization.ColumnChart(document.getElementById('id_chart'));
            var data_table = new google.visualization.DataTable(data);
            if (data_table.getNumberOfRows() > 0) {
                chart.draw(data_table, options);
            } else {
                $('#chart').html('');
            }

            console.log("success"); // sanity check
        },

        // handle a non-successful response
        error : function(xhr, errmsg, err) {
            $('#results').html(
                "<div class='alert-box alert radius' data-alert>" +
                    "Oops! We have encountered an error: " + errmsg +
                    " <a href='#' class='close'>&times;</a>" +
                "</div>"
            );
            console.log(xhr.status + ": " + xhr.responseText);
        }
    });
};
// -->