from django.db import models
from functools import total_ordering

app_label = 'miscellaneous'

# Create your models here.
class EmployeeType(models.Model):
    key = models.CharField(
        max_length=4,
        unique=True
    )
    name = models.CharField(
        max_length=32,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        unique_together = (('key', 'name'),)
        verbose_name = 'Tipo de empleado'
        verbose_name_plural = 'Tipos de empleados'

    def __str__(self):
        return u'{:s}'.format(self.name)

class Gender(models.Model):
    name = models.CharField(
        max_length=16,
        unique=True
    )

    class Meta:
        app_label = app_label
        ordering = ['name']
        verbose_name = 'Género'
        verbose_name_plural = 'Géneros'

    def __str__(self):
        return u'{:s}'.format(self.name)

@total_ordering
class Period(models.Model):
    key = models.CharField(
        max_length=8,
        unique=True
    )

    start = models.DateField()
    end = models.DateField()

    class Meta:
        app_label = app_label
        ordering = ['key',]
        verbose_name = 'Periodo'
        verbose_name_plural = 'Periodos'

    def __str__(self):
        return u'{:s}'.format(self.key)

    # Ordering methods
    def _is_valid_operand(self, other):
        return (hasattr(other, 'start') and hasattr(other, 'end'))

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented

        return ((self.start, self.end) == (other.start, other.end))
    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented

        if self.start < other.start:
            return True
        else:
            return self.end < other.end