from django.contrib import admin

from miscellaneous.models import (
    EmployeeType,
    Gender,
    Period,
)

# Register your models here.
class EmployeeTypeAdmin(admin.ModelAdmin):
    list_display = ('key', 'name',)
    ordering = ('name',)
admin.site.register(EmployeeType, EmployeeTypeAdmin)

class GenderAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
admin.site.register(Gender, GenderAdmin)

class PeriodAdmin(admin.ModelAdmin):
    list_display = ('key', 'start', 'end',)
    ordering = ('start',)
admin.site.register(Period, PeriodAdmin)