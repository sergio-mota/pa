from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response

# Create your views here.
@login_required(login_url='/login/')
def index(request):
    return render_to_response('index.html')