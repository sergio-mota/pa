from django.conf.urls import url
from django.contrib.auth.views import (login, logout)

from miscellaneous.views import index

urlpatterns = [
    url(r'^$', index),

    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout),
]